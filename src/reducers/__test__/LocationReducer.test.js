import LocationReducer from '../LocationReducer';
import * as types from '../../actions/ActionTypes';

it('updates state to indicate isLoading on start of locations GET',() => {
  const testAction = {
    type: types.GET_LOCATIONS_START
  };
  expect(LocationReducer({}, testAction)).toEqual({
    isLoading: true,
    error: false,
    errors: null
  });
}
);

it('updates state with an array of locations on successful locations GET',() => {
  const testAction = {
    type: types.GET_LOCATIONS_SUCCESS,
    payload: [{
      id: '12345'
    }]
  };
  expect(LocationReducer({}, testAction)).toEqual({
    isLoading: false,
    locations: {
      '12345':  { id: '12345' }
    }
  });
}
);

//TODO - add test for ERROR action
