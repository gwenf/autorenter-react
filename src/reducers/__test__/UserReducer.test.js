import UserReducer from '../UserReducer';

const testLoginAction = {
  type:'LOGGED_IN_SUCCESS',
  payload: {
    username: 'Test',
    password: 'Secret'
  }
};

const testLogoutAction = {
  type:'LOGGED_OUT_SUCCESS'
};

it('updates state with a user name on login',() => {
  expect(UserReducer({}, testLoginAction)).toEqual({
    isAuthenticated: true,
    user: {
      username: 'Test',
      password: 'Secret'
    },
    loading: false
  });
}
);

it('updates state with a user name on logout',() => {
  expect(UserReducer({}, testLogoutAction)).toEqual({
    user: null,
    isAuthenticated: false
  });
}
);

