import VehicleReducer from '../VehicleReducer';
import * as types from '../../actions/ActionTypes';

it('updates state to indicate isLoading and locationId on start of vehicles GET',() => {
  const testAction = {
    type: types.GET_VEHICLES_START,
    payload: '12345'
  };
  expect(VehicleReducer({}, testAction)).toEqual({
    isLoading: true,
    location: '12345',
    errors: null,
    error: false
  });
}
);

it('updates state with an array of vehicles on successful vehicles GET',() => {
  const testAction = {
    type: types.GET_VEHICLES_SUCCESS,
    payload: [{
      id: '12345',
      vin: 'ABCDE'
    }]
  };
  expect(VehicleReducer({}, testAction)).toEqual({
    isLoading: false,
    vehicles: {
      '12345': { id: '12345', vin: 'ABCDE' }
    }
  });
}
);
// TODO - test for error action
