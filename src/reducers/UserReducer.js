const UserReducer = (state = {}, action) => {
  switch(action.type) {
  case 'LOGGING_IN':
    return {
      ...state,
      error: null,
      loading: true
    };
  case 'LOGGED_IN_SUCCESS':
    return {
      ...state, 
      user: action.payload,
      isAuthenticated: true,
      loading: false
    };
  case 'LOGGED_IN_ERROR':
    return {
      ...state,
      loading: false,
      error: action.error
    };
  case 'LOGGED_OUT_SUCCESS':
    return {
      ...state,
      user: null,
      isAuthenticated: false
    };
  default:
    return state;
  }
};

export default UserReducer;

