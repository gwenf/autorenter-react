import * as types from '../actions/ActionTypes';

const initialState = {
  location: '',
  vehicles: {},
  isLoading: false,
  error: false
};

const VehicleReducer = (state = initialState, action) => {
  switch(action.type) {
  case types.GET_VEHICLES_START:
    return {
      ...state,
      location: action.payload,
      isLoading: true,
      error: false,
      errors: null
    };
  case types.GET_VEHICLES_SUCCESS:
    return {
      ...state,
      vehicles: action.payload.reduce((vehicles, vehicle) => {
        vehicles[vehicle.id] = vehicle;
        return vehicles;
      }, {}),
      isLoading: false
    };
  case types.GET_VEHICLES_ERROR:
    return {
      ...state,
      isLoading: false,
      error: true,
      errors: action.payload
    };
  case types.GET_VEHICLE_START:
    return {
      ...state,
      isLoading: true,
      error: false,
      errors: null
    };
  case types.GET_VEHICLE_SUCCESS:
    return {
      ...state,
      isLoading: false,
      vehicle: action.vehicle
    };
  case types.GET_VEHICLE_ERROR:
    return {
      ...state,
      isLoading: false,
      error: true,
      errors: action.error
    };

  default:
    return state;
  }
};
export default VehicleReducer;
