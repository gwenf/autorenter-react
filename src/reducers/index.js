import {reducer as toastrReducer} from 'react-redux-toastr';
import { reducer as formReducer } from 'redux-form';
import UserReducer from './UserReducer';
import LocationReducer from './LocationReducer';
import VehicleReducer from './VehicleReducer';
import { combineReducers } from 'redux';

const reducers = combineReducers({
  toastr: toastrReducer,
  form: formReducer,
  userData: UserReducer,
  locationData: LocationReducer,
  vehicleData: VehicleReducer
});

export default reducers;
