import * as types from '../actions/ActionTypes';

const initialState = {
  locations: {},
  isLoading: false,
  error: false
};

const locationReducer = (state = initialState, action) => {
  switch(action.type) {
  case types.GET_LOCATIONS_START:
    return {
      ...state,
      isLoading: true,
      error: false,
      errors: null
    };
  case types.GET_LOCATIONS_SUCCESS:
    return {
      ...state,
      locations: action.payload.reduce((locations, location) => {
        locations[location.id] = location;
        return locations;
      }, {}),
      isLoading: false
    };
  case types.GET_LOCATIONS_ERROR:
    return {
      ...state,
      isLoading: false,
      error: true,
      errors: action.payload
    };
  case types.GET_LOCATION_START:
    return {
      ...state,
      isLoading: true,
      error: false,
      errors: null
    };
  case types.GET_LOCATION_SUCCESS:
    return {
      ...state,
      isLoading: false,
      location: action.location
    };
  case types.GET_LOCATION_ERROR:
    return {
      ...state,
      isLoading: false,
      error: true,
      errors: action.error
    };
  case types.UPDATE_LOCATION_START:
    return {
      ...state,
      isLoading: true,
      error: false,
      errors: null
    };
  case types.UPDATE_LOCATION_SUCCESS:
    return {
      ...state,
      location: action.location,
      isLoading: false
    };
  case types.UPDATE_LOCATION_ERROR:
    return {
      ...state,
      error: true,
      errors: action.error,
      isLoading: false
    };
  case types.DELETE_LOCATION_START:
    return {
      ...state,
      isLoading: true,
      error: false,
      errors: null
    };
  case types.DELETE_LOCATION_SUCCESS:
    var locations = { ...state.locations };
    delete locations[action.locationId];  
    return {
      ...state,
      locations,
      isLoading: false
    };
  case types.DELETE_LOCATION_ERROR:
    return {
      ...state,
      error: true,
      errors: action.error,
      isLoading: false
    };
  case types.GET_STATES_SUCCESS:
    return {
      ...state,
      states: action.states
    };
  default:
    return state;
  }
};

export default locationReducer;

