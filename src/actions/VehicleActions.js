import * as axios from 'axios';
import * as types from './ActionTypes';
import * as actionMethods from './AutoRenterActions';

export const getVehicles = (locationId) => {
  const CALL_ROUTE = `${process.env.API_URL}locations/${locationId}/vehicles`;

  return (dispatch, getState) => {
    dispatch({
      type: types.GET_VEHICLES_START,
      payload: locationId
    });
    const state = getState();
    return axios.get(CALL_ROUTE, { headers: { Authorization: state.userData.user.token }}).then(
      (axiosData) => {
        dispatch({
          type: types.GET_VEHICLES_SUCCESS,
          payload: axiosData.data.vehicles
        });
      }).catch(
      (axiosError) => {
        actionMethods.handleErrror(axiosError, types.GET_VEHICLES_ERROR, dispatch);
      }    
    );
  };
};

export const getVehicle = (vehicleId) => {
  const CALL_ROUTE = `${process.env.API_URL}vehicles/${vehicleId}`;

  return (dispatch, getState) => {
    dispatch({
      type: types.GET_VEHICLE_START,
      payload: vehicleId
    });
    const state = getState();
    return axios.get(CALL_ROUTE, { headers: { Authorization: state.userData.user.token }}).then(
      (axiosData) => {
        dispatch({
          type: types.GET_VEHICLE_SUCCESS,
          vehicle: axiosData.data.vehicle
        });
      }).catch(
      (axiosError) => {
        actionMethods.handleErrror(axiosError, types.GET_VEHICLE_ERROR, dispatch);
      }    
    );
  };
};
