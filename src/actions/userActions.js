import axios from 'axios';

export const loggingIn = () => {
  return {
    type: 'LOGGING_IN'
  };
};

export const loggedInSuccess = (user) => {
  return {
    type: 'LOGGED_IN_SUCCESS',
    payload: user
  };
};

export const loggedInError = (errorMessage) => {
  return {
    type: 'LOGGED_IN_ERROR',
    error: errorMessage
  };
};

export const logoutSuccess = () => {
  return {
    type: 'LOGGED_OUT_SUCCESS'
  };
};

export const userLogin = (user, history) => {
  return (dispatch) => {
    dispatch(loggingIn());
    const CALL_ROUTE = process.env.API_URL + 'login/';
    
    axios.post(CALL_ROUTE, { ...user })
    .then(({ data }) => {
      localStorage.setItem('username', data.username);
      localStorage.setItem('token', data.token);
      return data;
    })
    .then((userData) => {
      return dispatch(loggedInSuccess(userData));
    })
    .then(() => history.push('fleet') )
    .catch((err) => {
      console.log(err);
      return dispatch(loggedInError(err));
    });
  };
};

