import * as axios from 'axios';
import * as actions from './LocationActions';
import * as types from './ActionTypes';
import * as actionMethods from './AutoRenterActions';
import { toastr } from 'react-redux-toastr';
const CALL_ROUTE = process.env.API_URL + 'locations/';

export const getLocations = () => {

  return (dispatch, getState) => {
    dispatch({type: types.GET_LOCATIONS_START});
    const state = getState();

    return axios.get(CALL_ROUTE, { headers: { Authorization: state.userData.user.token }}).then(
      (axiosData) => {
        dispatch({
          type: types.GET_LOCATIONS_SUCCESS,
          payload: axiosData.data.locations
        });
      }).catch(
        (axiosError) => {
          console.log(axiosError);
          actionMethods.handleErrror(axiosError, types.GET_LOCATIONS_ERROR, dispatch);
        }    
    );
  };
};

export const getLocation = (locationId) => {

  return (dispatch, getState) => {
    dispatch({type: types.GET_LOCATION_START});
    const state = getState();

    return axios.get(CALL_ROUTE + locationId, { headers: { Authorization: state.userData.user.token }})
      .then((res) => {
        dispatch(actions.getLocationSuccess(res.data.location));
      })
      .catch((err) => {
        console.log(err);
        dispatch({type: types.GET_LOCATION_ERROR});
      });
  };
};

export const addNewLocation = (location, history) => {
  return (dispatch, getState) => {
    dispatch(actions.addLocationStart());
    const state = getState();

    const postConfig = {
      method: 'POST',
      url: CALL_ROUTE,
      data: location,
      headers: {
        Authorization: state.userData.user.token
      }
    };

    return axios(postConfig)
      .then(() => {
        return dispatch(actions.addLocationSuccess());
      })
      .then(() => toastr.success('SUCCESS', 'Location added successfully!', {timeOut: 5000}))
      .then(() => history.push('fleet/locations'))
      .catch((err) => {
        return dispatch(actions.addLocationError(err));
      });
  };
};

export const updateLocation = (data, history) => {
  return (dispatch, getState) => {
    dispatch(actions.updateLocationStart());
    const state = getState();

    const putConfig = {
      method: 'PUT',
      url: CALL_ROUTE + data.locationId,
      data: data.location,
      headers: {
        Authorization: state.userData.user.token
      }
    };

    return axios(putConfig)
      .then(() => {
        return dispatch(actions.updateLocationSuccess());
      })
      .then(() => toastr.success('SUCCESS', 'Location updated successfully!', {timeOut: 5000}))
      .then(() => history.push('fleet/locations'))
      .catch((err) => {
        return dispatch(actions.updateLocationError(err));
      });
  };
};

export const deleteLocation = (locationId) => {
  return (dispatch, getState) => {
    dispatch(actions.deleteLocationStart());
    const state = getState();

    const deleteConfig = {
      method: 'DELETE',
      url: CALL_ROUTE + locationId, 
      headers: {
        Authorization: state.userData.user.token
      }
    };

    return axios(deleteConfig)
      .then(() => {
        dispatch(actions.deleteLocationSuccess(locationId));
      })
      .catch((err) => {
        console.log(err);
        dispatch(actions.deleteLocationError(err));
      });
  };
};

export const getStates = () => {
  return (dispatch, getState) => {
    const state = getState();

    axios.get(`${process.env.API_URL}lookup-data?states`, { headers: { Authorization: state.userData.user.token }})
      .then((res) => {
        dispatch(actions.getStatesSuccess(res.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

