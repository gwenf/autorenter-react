export const handleErrror = (axiosError, actionType, dispatch) => {
  let errorMessage = axiosError.toString();
  if (axiosError.response != undefined
    && axiosError.response.data != undefined) {
    errorMessage = axiosError.response.data;
  }
  dispatch({
    type: actionType,
    payload: errorMessage
  });
};


