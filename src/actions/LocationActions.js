import * as types from './ActionTypes';

export const getLocationSuccess = (location) => {
  return {
    type: types.GET_LOCATION_SUCCESS,
    location
  };
};

export const addLocationStart = () => {
  return {
    type: types.ADD_LOCATION_START
  };
};

export const addLocationSuccess = (location) => {
  return {
    type: types.ADD_LOCATION_SUCCESS,
    location
  };
};

export const addLocationError = (err) => {
  return {
    type: types.ADD_LOCATION_ERROR,
    error: true,
    errors: err
  };
};

export const updateLocationStart = () => {
  return {
    type: types.UPDATE_LOCATION_START
  };
};

export const updateLocationSuccess = (location) => {
  return {
    type: types.UPDATE_LOCATION_SUCCESS,
    location
  };
};

export const updateLocationError = (err) => {
  return {
    type: types.UPDATE_LOCATION_ERROR,
    error: true,
    errors: err
  };
};

export const deleteLocationStart = () => {
  return {
    type: types.DELETE_LOCATION_START
  };
};

export const deleteLocationSuccess = (locationId) => {
  return {
    type: types.DELETE_LOCATION_SUCCESS,
    locationId
  };
};

export const deleteLocationError = (err) => {
  return {
    type: types.DELETE_LOCATION_ERROR,
    error: true,
    errors: err
  };
};

export const getStatesSuccess = (states) => {
  return {
    type: types.GET_STATES_SUCCESS,
    states
  };
};

