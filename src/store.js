import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import persist from 'redux-persist-to-localstorage';
import reducers from './reducers';

const middleware = [thunk];

const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose;

// select is the state that will be persisted to local storage
const select = (state) => {
  if (state.userData.isAuthenticated) {
    localStorage.removeItem('username');
    localStorage.removeItem('token');
  }
  return {...state};
};

// receive is the state that will be retrieved from local storage
const receive = (state) => {  
  return {...state};
};

const enhancer = composeEnhancers(
  applyMiddleware(...middleware),
  persist('autoRenterState', select, receive)
);

const appStore = (initialState) => {
  const state = JSON.parse(localStorage.getItem('autoRenterState')) || initialState;
  return createStore(
    reducers, 
    state,
    enhancer
  );
};

export default appStore;
