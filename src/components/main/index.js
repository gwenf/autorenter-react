import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import registerServiceWorker from '../../registerServiceWorker';
import '../../assets/css/index.css';
import App from './App';
import { Provider } from 'react-redux';
import appStore from '../../store';

let storeInstance = appStore();

ReactDOM.render(
  <Provider store={storeInstance}>
    <BrowserRouter store={storeInstance}>
      <App store={storeInstance} />
    </BrowserRouter>
  </Provider>
  , document.getElementById('root')
);

registerServiceWorker();

