import React, { Component } from 'react';
import ReduxToastr from 'react-redux-toastr';
import HeaderContainer from '../common/HeaderContainer';
import styles from './App.css';
import AppRoutes from '../routes/AppRoutes';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { store, match } = this.props;
    return (
      <div className={`App ${styles.App}`}>
        <HeaderContainer />
        <Row>
          <Col sm={10} smOffset={1}>
            <AppRoutes store={store} match={match} />
          </Col>
        </Row>
        <ReduxToastr
          timeOut={0}
          newestOnTop={false}
          preventDuplicates
          position="top-right"
          transitionIn="fadeIn"
          transitionOut="fadeOut"
          progressBar
        />
      </div>
    );
  }
}
App.propTypes = {
  store: PropTypes.object,
  match: PropTypes.object
};

export default App;
