import React from 'react';
import renderer from 'react-test-renderer';
import {StaticRouter} from 'react-router-dom';
import NavBar from '../common/NavBar';

const testUser = {
  firstName: '',
  lastName: ''
}; 

it('renders correctly', () => {
  const context={};
  const app = renderer.create(
    <StaticRouter location="/" context={context}>  
      <NavBar user={testUser}/>
    </StaticRouter>
  ).toJSON();
  expect(app).toMatchSnapshot();
});