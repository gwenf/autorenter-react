import React from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';

const ErrorMessage = (props) => {
  const { message } = props;
  return (
    <Alert bsStyle="danger">{message}</Alert>
  );
};

ErrorMessage.propTypes = {
  message: PropTypes.string
};

export default ErrorMessage;
