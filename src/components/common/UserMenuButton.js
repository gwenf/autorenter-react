import React from 'react';
import PropTypes from 'prop-types';
import { DropdownButton, MenuItem } from 'react-bootstrap';

const UserMenuButton = (props) => {
  const { user, logout } = props;
  return (
    <DropdownButton
      title={user.username}
      id={`usermenu-dropdown-${user.username}`}
      style={{marginTop: '8px'}}
    >
      <MenuItem eventKey="1" onSelect={logout}>
        Logout
      </MenuItem>
    </DropdownButton>
  );
};

UserMenuButton.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func
};

export default UserMenuButton;