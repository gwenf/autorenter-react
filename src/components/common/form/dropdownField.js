import React from 'react';
import PropTypes from 'prop-types';
import { FormControl } from 'react-bootstrap';

const DropDownSelect = ({ input }) => {
  return (
		<div>
			<FormControl componentClass="select" placeholder="select" {...input}>
      <option disabled hidden value="">Select</option>
      </FormControl>
		</div>
  );
};

DropDownSelect.propTypes = {
  input: PropTypes.object,
  states: PropTypes.array
};

export default DropDownSelect;

