import React from 'react';
import PropTypes from 'prop-types';
import styles from './fieldStyles.css';

const inputField = ({ input, type, meta: { touched, error } }) => {
  return (
  <div>
    <input {...input} className="form-control" type={type || 'text'} />
    {touched && error && <span className={styles.formError}>{error}</span>}
  </div>
  );
};

inputField.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string,
  meta: PropTypes.object
};

export default inputField;
