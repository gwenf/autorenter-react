import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as UserActions from '../../actions/userActions';
import PropTypes from 'prop-types';
import NavBar from './NavBar';

class HeaderContainer extends Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
  }

  logout() {
    this.props.action.logoutSuccess();
    this.props.history.push('/login');
  }

  render() {
    const { user, isAuth } = this.props;
    return (
      <NavBar isAuth={isAuth} user={user} logout={this.logout}/>
    );
  }
}

HeaderContainer.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string,
    password: PropTypes.string
  }),
  action: PropTypes.object,
  isAuth: PropTypes.bool,
  history: PropTypes.object
};

const mapStateToProps = (state) => {
  return {
    user: state.userData.user,
    isAuth: state.userData.isAuthenticated
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    action: bindActionCreators(UserActions, dispatch)
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HeaderContainer));

