import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {Navbar, Nav, NavItem} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import styles from './NavBar.css';
import UserMenuButton from './UserMenuButton';

const NavBar = (props) => {
  const {user, isAuth, logout} = props;
  const {logo, btnOutline, padAdjust} = styles;
  return (
    <div>
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="https://www.fusionalliance.com" className={logo}>
              <img src={require('../../assets/img/fusion_dots.png')} className="logo" alt=""/>
              <img src={require('../../assets/img/fusion_name.png')} className="logo" alt="Fusion Alliance"/>
            </a>
          </Navbar.Brand>
        </Navbar.Header>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">AutoRenter</Link>
          </Navbar.Brand>
        </Navbar.Header>

        {
          isAuth ?
            <Nav pullRight>
              <LinkContainer to="/admin"><NavItem>Admin</NavItem></LinkContainer>
              <LinkContainer to="/fleet"><NavItem>Fleet</NavItem></LinkContainer>
              <LinkContainer to="/tech-support"><NavItem>Tech Support</NavItem></LinkContainer>
                <UserMenuButton 
                  logout={logout} 
                  className={`btn ${btnOutline}`}
                  user={user}
                />
            </Nav> :
            <Nav pullRight>
              <LinkContainer to="/login">
                <NavItem className={padAdjust}>
                  <div className={`btn ${btnOutline}`}>Login</div>
                </NavItem>
              </LinkContainer>
            </Nav>
        }
      </Navbar>
    </div>
  );
};

NavBar.propTypes = {
  user: PropTypes.object,
  isAuth: PropTypes.bool,
  logout: PropTypes.func
};

export default NavBar;
