import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import styles from './AddNewButton.css';

const AddNewButton = (props) => {
  const { match } = props;

  return (
    <nav>
      <NavLink to={`${match.url}/add`} className={`${styles.floatRight} ${styles.paddingTopBottom}`}>
        <Button bsStyle="primary" >New</Button>
      </NavLink>
    </nav>
  );
};

AddNewButton.propTypes = {
  match: PropTypes.object
};

export default AddNewButton;