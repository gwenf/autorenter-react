import React from 'react';
import PropTypes from 'prop-types';
import { Link, Route } from 'react-router-dom';
import capitalize from 'capitalize';
import styles from './breadcrumbs.css';

const BreadcrumbItem = ({ match, nameResolvers = {}, nameResolver = capitalize }) => {
  const name = nameResolver(match.params.path);
  return [
    <li
      key={match.url}
      className={styles.breadcrumbItem}
      aria-current={match.isExact ? 'page' : undefined}>
      { match.isExact ? name : <Link to={match.url || ''}>{name}</Link> }
    </li>,
    <Route
      key={`route-${match.url}`}
      path={`${match.url}/:path`}
      render={(props) => (
        <BreadcrumbItem
          {...props}
          nameResolvers={nameResolvers}
          nameResolver={nameResolvers[match.params.path]}
        />
      )}
    />
  ];
};

BreadcrumbItem.propsTypes = {
  match: PropTypes.shape({
    url: PropTypes.string.isRequired,
    isExact: PropTypes.bool,
    params: PropTypes.object.isRequired
  }).isRequired,
  nameResolvers: PropTypes.object,
  nameResolver: PropTypes.func
};

export default BreadcrumbItem;
