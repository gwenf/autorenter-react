import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import { Well } from 'react-bootstrap';
import BreadcrumbItem from './BreadcrumbItem';
import styles from './breadcrumbs.css';

const Breadcrumbs = ({ path = '', nameResolvers = {} }) => (
  <Well bsSize="small">
    <nav className="breadcrumbs">
      <ol className={styles.breadcrumbsList}>
        <Route path={`${path}/:path`} render={(props) =>
          <BreadcrumbItem {...props} nameResolvers={nameResolvers} />
        } />
      </ol>
    </nav>
  </Well>
);

Breadcrumbs.propTypes = {
  path: PropTypes.string,
  nameResolvers: PropTypes.object
};

export default Breadcrumbs;
