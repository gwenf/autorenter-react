import React, { Component } from 'react';
import { Grid, Tab, Tabs } from 'react-bootstrap';

class Admin extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Grid fluid>
        <Tabs defaultActiveKey={1} id="admin-tabs">
          <Tab eventKey={1} title="Users"><h2>Admin Users Panel</h2></Tab>
          <Tab eventKey={2} title="Branding"><h2>Admin Branding Panel</h2></Tab>
        </Tabs>
      </Grid>
    );
  }
}

export default Admin;
