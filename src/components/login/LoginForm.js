import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Form, FormGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import styles from './loginStyles.css';
import inputField from '../common/form/inputField';
import validate from './loginValidation';

let LoginForm = ({ submissionError, handleSubmit, pristine, submitting, hasUsername, hasPassword }) => {
  return (
    <Row className={styles.loginContainer}>
      <Form onSubmit={handleSubmit} horizontal>
        <legend>Login</legend>
        {
          submissionError ?
            <div className={styles.loginError}>
              There was a problem logging in. Please check your credentials and try again.
            </div> :
            ''
         }
        <FormGroup controlId="formHorizontalUsername">
          <Col sm={2}>
            Username 
          </Col>
          <Col sm={10}>
            <Field
              name="username"
              component={inputField}
              required
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="formHorizontalPassword">
          <Col sm={2}>
            Password
          </Col>
          <Col sm={10}>
            <Field
              name="password"
              component={inputField}
              required
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <button type="submit" className="btn btn-default" disabled={!hasUsername || !hasPassword || pristine || submitting}>
              Sign in
            </button>
          </Col>
        </FormGroup>
      </Form>
    </Row>
  );
};

LoginForm.propTypes = {
  onFormChange: PropTypes.func,
  onFormSubmit: PropTypes.func,
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
  pristine: PropTypes.bool,
  hasUsername: PropTypes.string,
  hasPassword: PropTypes.string,
  submissionError: PropTypes.object
};

LoginForm = reduxForm({
  form: 'loginForm',
  validate
})(LoginForm);

const selector = formValueSelector('loginForm');
LoginForm = connect(state => {
  const submissionError = state.userData.error;
  const hasUsername = selector(state, 'username');
  const hasPassword = selector(state, 'password');
  return {
    hasUsername,
    hasPassword,
    submissionError
  };
})(LoginForm);

export default LoginForm;

