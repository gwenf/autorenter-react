import React from 'react';
import PropTypes from 'prop-types';
import { Grid } from 'react-bootstrap';
import LoginForm from './LoginForm';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { DotLoader as Loader } from 'react-spinners';
import * as UserActions from '../../actions/userActions';
import styles from './loginStyles.css';

class LoginContainer extends React.Component{
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(formValues) {
    this.props.action.userLogin(formValues, this.props.history);
  }

  render() {
    const { userData } = this.props;
    return (
      <Grid>
        {
          userData.loading ?
            <div className={styles.loadingOverlay} /> :
            ''
        }
        <Loader loading={!!userData.loading} className={styles.loaderStyles} />
        <LoginForm onSubmit={this.onSubmit} error={userData.error} />
      </Grid>
    );
  }
}

LoginContainer.propTypes = {
  action: PropTypes.object,
  'action.userLogin': PropTypes.func,
  history: PropTypes.object,
  userData: PropTypes.shape({
    loading: PropTypes.bool
  })
};

const mapStateToProps = (state) => {
  return {
    userData: state.userData
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    action: bindActionCreators(UserActions, dispatch)
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginContainer));

