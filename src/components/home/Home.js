import React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

const HomeContainer = () => {
  return (
    <Grid fluid>
      <Row>
        <Col xs={12} sm={4}>
          <h2>Fusion Alliance</h2>
          <p>
            Some company specific marketing blurb goes here... Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
          </p>
        </Col>

        <Col xs={12} sm={8}>
          <a>
            <img
              src={require('../../assets/img/mercedes_s_class.jpg')}
              width="100%"
              alt="Mercedes S Class"
            />
          </a>
        </Col>
      </Row>
    </Grid>
  );
};

export default HomeContainer;
