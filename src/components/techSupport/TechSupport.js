import React, { Component } from 'react';
import { toastr } from 'react-redux-toastr';
import styles from './techSupport.css';

class TechSupport extends Component {
  render() {
    return (
      <div className={styles.TechSupport}>
        <h2>Tech Support</h2>
        <p className={styles.SectionLabel}>Notify</p>
        <div className={styles.BtnContainer}>
          <button
            className={`${styles.BtnCustom} btn btn-danger`}
            onClick={() => toastr.error('ERROR', 'There was an error!')}
          >
            Error
          </button>
          <button
            className={`${styles.BtnCustom} btn btn-warning`}
            onClick={() => toastr.warning('WARNING', 'Somthing Happened...')} 
          >
            Warning
          </button>
          <button
            className={`${styles.BtnCustom} btn btn-info`}
            onClick={() => toastr.info('INFO', 'Info about something.')} 
          >
            Info
          </button>
          <button
            className={`${styles.BtnCustom} btn btn-success`}
            onClick={() => toastr.success('SUCCESS', 'Successfull!!!', {timeOut: 5000})} 
          >
            Success
          </button>
        </div>
        <p className={styles.SectionLabel}>Test Exception</p>
        <div className={styles.BtnContainer}>
          <button
            className="btn btn-danger"
            onClick={() => toastr.error('EXCEPTION', 'An exception...')}
          >
            Throw Exception
          </button>
        </div>
        <p className={styles.SectionLabel}>Server Error</p>
        <div className={styles.BtnContainer}>
          <button
            className="btn btn-danger"
            onClick={() => toastr.error('HTTP ERROR', 'There was an HTTP related error.')}
          >
            HTTP Error
          </button>
        </div>
      </div>
    );
  }
}

export default TechSupport;
