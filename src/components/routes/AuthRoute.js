import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

class AuthRoute extends Component {
  render() {
    const { isAuth, component: Component, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={(props) => isAuth === true
          ? <Component {...props} />
          : <Redirect to={{pathname: '/login'}} />}
      />
    );
  }
}

AuthRoute.propTypes = {
  isAuth: PropTypes.bool,
  component: PropTypes.func
};

const mapStateToProps = (state) => {
  return { isAuth: state.userData.isAuthenticated };
};

export default connect(mapStateToProps)(AuthRoute);

