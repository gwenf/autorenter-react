import React from 'react';
import { Switch, Route } from 'react-router-dom';
import FleetView from '../fleet/FleetView';
import Admin from '../admin/Admin';
import LoginContainer from '../login/LoginContainer';
import TechSupport from '../techSupport/TechSupport';
import HomeContainer from '../home/Home';
import AuthRoute from './AuthRoute';

const AppRoutes = () => {
  return (
    <Switch>
      <Route exact path="/" component={HomeContainer} />
      <AuthRoute path="/admin" component={Admin} />
      <AuthRoute path="/fleet" component={FleetView} />
      <AuthRoute path="/tech-support" component={TechSupport} />
      <Route path="/login" component={LoginContainer} />
    </Switch>
  );
};

export default AppRoutes;
