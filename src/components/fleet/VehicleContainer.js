import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as VehicleActions from '../../actions/VehicleActions';
import VehicleList from './VehicleList';
import AddNewButton from '../common/AddNewButton';

class VehicleContainer extends Component{
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { match, action } = this.props;
    action.getVehicles(match.params.locationId);
  }

  render() {
    const { vehicleData, match } = this.props;
    const { params: {locationId}} = match;

    if (vehicleData.errors != undefined) {
      console.log(vehicleData.errors);
    }

    return (
      <Row>
        <Col lg={12}>
          <AddNewButton match={match}/>
          <VehicleList vehicleData={vehicleData} locationId={locationId} />
        </Col>
      </Row>
    );
  }
}

VehicleContainer.propTypes = {
  action: PropTypes.object.isRequired,
  vehicleData: PropTypes.shape({
    isLoading: PropTypes.bool.isRequired,
    errors: PropTypes.array,
    vehicles: PropTypes.object
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      locationId: PropTypes.string
    })
  })
};

const mapStateToProps = (state) => {
  return {
    vehicleData: state.vehicleData
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    action: bindActionCreators(VehicleActions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(VehicleContainer);
