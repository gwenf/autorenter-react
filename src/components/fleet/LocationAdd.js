import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import LocationUpsertForm from './LocationUpsertForm';
import * as LocationActions from '../../actions/LocationGenerators';

// TODO: clear the form on unmount
class LocationAdd extends Component {
  constructor () {
    super();
    this.onSubmit = this.onSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }
  onSubmit (location) {
    this.props.action.addNewLocation(location, this.props.history);
    this.props.action.getStates();
  }
  onCancel () {
    this.props.history.push('fleet/locations');
  }
  render () {
    return (
      <div>
        <LocationUpsertForm
          action="Add New"
          states={this.props.states}
          handleCancel={this.onCancel}
          onSubmit={this.onSubmit}
        />
      </div>
    );
  }
}

LocationAdd.propTypes = {
  history: PropTypes.object,
  states: PropTypes.object,
  action: PropTypes.shape({
    addNewLocation: PropTypes.func,
    getStates: PropTypes.func
  })
};

const mapStateToProps = (state) => {
  return {
    states: state.locationData.states
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    action: bindActionCreators(LocationActions, dispatch)
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LocationAdd));

