import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as LocationActions from '../../actions/LocationGenerators';
import LocationList from './LocationList';
import AddNewButton from '../common/AddNewButton';

class LocationContainer extends Component{
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { action } = this.props;
    action.getLocations();
  }

  render() {
    const { locationData, routes, match } = this.props;

    if (locationData.errors != undefined) {
      console.log(locationData.errors);
    }

    return (
      <div>
        <AddNewButton routes={routes} match={match} />
        <LocationList locationData={locationData} routes={routes} match={match}/>
      </div>
    );
  }
}

LocationContainer.propTypes = {
  action: PropTypes.object.isRequired,
  locationData: PropTypes.shape({
    isLoading: PropTypes.bool.isRequired,
    errors: PropTypes.any,
    locations: PropTypes.object
  }).isRequired,
  routes: PropTypes.array,
  match: PropTypes.object
};

const mapStateToProps = (state) => {
  return {
    locationData: state.locationData
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    action: bindActionCreators(LocationActions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LocationContainer);

