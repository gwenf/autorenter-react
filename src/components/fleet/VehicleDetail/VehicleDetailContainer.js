
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as VehicleActions from '../../../actions/VehicleActions';
import VehicleDetail from './VehicleDetail';
import Loader from 'react-loader';
import ErrorMessage from '../../common/ErrorMessage';


class VehicleDetailContainer extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { action, match } = this.props;
    action.getVehicle(match.params.vehicleId);
  }

  render() {
    const { match, vehicleData } = this.props;
    return (
      <Loader loaded={!vehicleData.isLoading}>
        {vehicleData.error ?
          <ErrorMessage message={vehicleData.errors}/> :
          <VehicleDetail vehicleData={vehicleData} match={match}/>
        }
      </Loader>
    );
  }
}

VehicleDetailContainer.propTypes = {
  action: PropTypes.object,
  match: PropTypes.shape({
    params: PropTypes.shape({
      vehicleId: PropTypes.string
    })
  }),
  vehicleData: PropTypes.object
};

const mapStateToProps = (state) => {
  return {
    vehicleData: state.vehicleData
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    action: bindActionCreators(VehicleActions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(VehicleDetailContainer);
