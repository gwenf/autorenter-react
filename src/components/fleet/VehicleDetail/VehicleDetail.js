import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import {Button, Col, Row} from 'react-bootstrap';

import style from './vehicle.css';

class VehicleDetail extends Component {

  render() {
    const {match, vehicleData} = this.props;
    const {vehicle} = vehicleData;

    return (
      <div>
        <h3>{`Vehicle Detail: ${vehicle.vin}`}</h3>
        <Row>
          <Col xs={6}>
            <Row>
              <Col xs={5}>
                <p>VIN</p>
              </Col>
              <Col xs={5}>
                <p>{vehicle.vin}</p>
              </Col>
            </Row>
            <Row>
              <Col xs={5}>
                <p>Make</p>
              </Col>
              <Col xs={5}>
                <p>{vehicle.make}</p>
              </Col>
            </Row>
            <Row>
              <Col xs={5}>
                <p>Model</p>
              </Col>
              <Col xs={5}>
                <p>{vehicle.model}</p>
              </Col>
            </Row>
            <Row>
              <Col xs={5}>
                <p>Year</p>
              </Col>
              <Col xs={5}>
                <p>{vehicle.year}</p>
              </Col>
            </Row>
            <Row>
              <Col xs={5}>
                <p>Miles</p>
              </Col>
              <Col xs={5}>
                <p>{vehicle.miles}</p>
              </Col>
            </Row>
            <Row>
              <Col xs={5}>
                <p>Color</p>
              </Col>
              <Col xs={5}>
                <p>{vehicle.color}</p>
              </Col>
            </Row>
            <Row>
              <Col xs={5}>
                <p>Rent to Own</p>
              </Col>
              <Col xs={5}>
                <p>{vehicle.vin}</p>
              </Col>
            </Row>
          </Col>
          <Col xs={6}>
            <img alt={vehicle.name} src={vehicle.image} width="100%"/>
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <NavLink
              className={`pull-right ${style.vehicleEdit}`}
              to={`${match.url}/edit`}
            >
              <Button bsStyle="primary">Edit</Button>
            </NavLink>
          </Col>
        </Row>
      </div>);
  }
}

VehicleDetail.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      vehicleId: PropTypes.string
    })
  }),
  vehicleData: PropTypes.object
};


export default VehicleDetail;
