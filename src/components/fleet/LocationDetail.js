import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { Button, Col, Row } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as LocationActions from '../../actions/LocationGenerators';

const LocationDetail = ({ locationData, match }) => {

  const {params: {locationId}} = match;
  const location = locationData.locations[locationId];
  
  return (
    <div>
      <h3>{`Location Detail: ${location.name}`}</h3>
      <Row>
        <Col xs={2}>
          <p>Site ID</p>
        </Col>
        <Col xs={2}>
          <p>{location.siteId}</p>
        </Col>
      </Row>
      <Row>
        <Col xs={2}>
          <p>City</p>
        </Col>
        <Col xs={2}>
          <p>{location.city}</p>
        </Col>
      </Row>
      <Row>
        <Col xs={2}>
          <p>State</p>
        </Col>
        <Col xs={2}>
          <p>{location.stateCode}</p>
        </Col>
      </Row>
      <Row>
        <Col xs={2}>
          <p>Vehicles</p>
        </Col>
        <Col xs={2}>
          <p>{location.vehicleCount}</p>
        </Col>
      </Row>
      <Row>
        <Col xs={10}>
          <NavLink
            className="pull-right"
            to={`${match.url}/edit`}
          >
            <Button bsStyle="primary" >Edit</Button>
          </NavLink>
        </Col>
      </Row>
    </div>
  );
};

LocationDetail.propTypes = {
  locationData: PropTypes.shape({
    isLoading: PropTypes.bool.isRequired,
    errors: PropTypes.any,
    location: PropTypes.object
  }).isRequired,
  match: PropTypes.object
};

const mapStateToProps = (state) => {
  return {
    locationData: state.locationData
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    action: bindActionCreators(LocationActions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LocationDetail);
