import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import LocationUpsertForm from './LocationUpsertForm';
import * as LocationActions from '../../actions/LocationGenerators';

// TODO: clear the form on unmount
class LocationEdit extends Component {
  constructor () {
    super();

    this.onSubmit = this.onSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  componentDidMount () {
    const locationId = this.props.match.params.locationId;
    this.props.action.getLocation(locationId);
    this.props.action.getStates();
  }

  onSubmit (location) {
    const data = {
      locationId: this.props.match.params.locationId,
      location: {
        ...location,
        id: this.props.location.id,
        vehicleCount: this.props.location.vehicleCount
      }
    };
    this.props.action.updateLocation(data, this.props.history);
  }

  onCancel () {
    this.props.history.push('fleet/locations');
  }

  render () {
    return (
      <div>
        <LocationUpsertForm
					location={this.props.location}
					action="Edit"
          states={this.props.states}
					handleCancel={this.onCancel}
					onSubmit={this.onSubmit}
				/>
      </div>
    );
  }
}

LocationEdit.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object,
  states: PropTypes.object,
  action: PropTypes.shape({
    addNewLocation: PropTypes.func,
    getLocation: PropTypes.func,
    updateLocation: PropTypes.func,
    getStates: PropTypes.func
  }),
  location: PropTypes.object
};

const mapStateToProps = (state) => {
  return {
    location: state.locationData.location,
    states: state.locationData.states
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    action: bindActionCreators(LocationActions, dispatch)
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LocationEdit));

