import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';
import ErrorMessage from '../common/ErrorMessage';
import Loader from 'react-loader';
import { Link } from 'react-router-dom';

const VehicleList = (props) => {
  const { vehicleData, locationId } = props;
  return (
    <Loader loaded={!vehicleData.isLoading}>
      <Table striped hover bordered condensed>
        <thead>
          <tr>
            <td >VIN</td>
            <td >Image</td>
            <td >Make</td>
            <td >Model</td>
            <td >Year</td>
            <td >Miles</td>
            <td >Color</td>
            <td >Rent to Own</td>
            <td >Actions</td>
          </tr>
        </thead>
        {vehicleData.error ?
          <ErrorMessage message={vehicleData.errors} /> :
          (<tbody>
            {Object.values(vehicleData.vehicles).map((vehicle) => {
              return (
                <tr key={vehicle.id}>
                  <td >{vehicle.vin}</td>
                  <td ><img alt="" src={vehicle.image} width="180px" /></td>
                  <td >{vehicle.make}</td>
                  <td >{vehicle.model}</td>
                  <td >{vehicle.year}</td>
                  <td >{vehicle.miles}</td>
                  <td >{vehicle.color}</td>
                  <td >{vehicle.isRentToOwn ? <span className="glyphicon glyphicon-ok" aria-hidden="true" /> : ''}</td>
                  <td >
                    <Link to={`/fleet/locations/${locationId}/vehicles/${vehicle.id}/edit`}>edit </Link>
                    <a>delete </a>
                  </td>
                </tr>);
            })
            }
          </tbody>)
        }
      </Table>
    </Loader>
  );
};

VehicleList.propTypes = {
  vehicleData: PropTypes.shape({
    isLoading: PropTypes.bool.isRequired,
    errors: PropTypes.any,
    locations: PropTypes.object
  }).isRequired,
  locationId: PropTypes.string
};

export default VehicleList;
