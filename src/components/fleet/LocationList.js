import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table, Button } from 'react-bootstrap';
import Modal from 'react-modal';
import ErrorMessage from '../common/ErrorMessage';
import Loader from 'react-loader';
import { Link } from 'react-router-dom';
import * as LocationActions from '../../actions/LocationGenerators';
import SortButton from './SortButton';
import style from './locations.css';

class LocationList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sortBy: 'id',
      ascending: true,
      isModalOpen: false,
      locationId: ''
    };

    this.sort = this.sort.bind(this);
    this.deleteLocation = this.deleteLocation.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  sort(sortBy) {
    console.log('sort', sortBy);
    this.setState({
      sortBy,
      ascending: this.state.sortBy === sortBy ? !this.state.ascending : true
    });
  }

  openModal (locationId) {
    this.setState({
      isModalOpen: true,
      locationId
    });
  }

  closeModal () {
    this.setState({
      isModalOpen: false
    });
  }

  deleteLocation () {
    this.setState({
      isModalOpen: false
    });
    this.props.action.deleteLocation(this.state.locationId);
  }

  render() {
    const { locationData, match } = this.props;
    const { sortBy, ascending } = this.state;
    const currentSort = { sortBy, ascending };
    const locations = Object.values(locationData.locations).sort(location => location[sortBy]);
    if(!ascending) {
      locations.reverse();
    }
    return (
      <Loader loaded={!locationData.isLoading}>
        <Table striped hover bordered condensed>
          <thead className={style.tableheader}>
            <tr>
              <th>
                <SortButton sortBy="id" onSort={this.sort} currentSort={currentSort}>
                  <span>Site ID</span>
                </SortButton>
              </th>
              <th>
                <SortButton sortBy="name" onSort={this.sort} currentSort={currentSort}>
                  Name
                </SortButton>
              </th>
              <th>
                <SortButton sortBy="vehicleCount" onSort={this.sort} currentSort={currentSort}>
                  Vehicles
                </SortButton>
              </th>
              <th>
                <SortButton sortBy="city" onSort={this.sort} currentSort={currentSort}>
                  City
                </SortButton>
              </th>
              <th>
                <SortButton sortBy="stateCode" onSort={this.sort} currentSort={currentSort}>
                  State
                </SortButton>
              </th>
              <th>
                Actions
              </th>
            </tr>
          </thead>
          {locationData.error ?
            <ErrorMessage message={locationData.errors} /> :
            (<tbody>
              {locations.map((location) => {
                return (
                  <tr key={location.id}>
                    <td ><Link to={`${match.path}/${location.id}`}>{location.siteId}</Link></td>
                    <td >{location.name}</td>
                    <td >{location.vehicleCount}</td>
                    <td >{location.city}</td>
                    <td >{location.stateCode}</td>
                    <td >
                      <Link to={`${match.path}/${location.id}/edit`}>Edit</Link>&nbsp;
                      <Link to={`${match.path}/${location.id}/vehicles`}>Vehicles</Link>&nbsp;
                      <a onClick={()=>this.openModal(location.id)} style={{ cursor: 'pointer' }}>Delete</a>
                    </td>
                  </tr>);
              })
              }
            </tbody>)
          }
        </Table>
        <Modal
          ariaHideApp={false}
          style={{content: {top: '30%', bottom: '40%', right: '35%', left: '35%'}}}
          isOpen={this.state.isModalOpen}
        >
          <h3>Confirm Delete</h3>
					<p>Are you sure you want to delete this location?</p>
					<Button onClick={this.closeModal}>Close</Button>
					<Button onClick={()=>this.deleteLocation()}>Delete</Button>
				</Modal>
			</Loader>
    );
  }
}

LocationList.propTypes = {
  locationData: PropTypes.shape({
    isLoading: PropTypes.bool.isRequired,
    errors: PropTypes.any,
    locations: PropTypes.object
  }).isRequired,
  routes: PropTypes.array,
  match: PropTypes.object,
  action: PropTypes.object
};

const mapStateToProps = () => {
  return {
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    action: bindActionCreators(LocationActions, dispatch)
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(LocationList);

