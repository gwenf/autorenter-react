import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Nav, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import LocationContainer from './LocationContainer';
import LocationAdd from './LocationAdd';
import LocationDetail from './LocationDetail';
import LocationEdit from './LocationEdit';
import VehicleContainer from './VehicleContainer';
import VehicleAdd from './VehicleAdd';
import VehicleDetailContainer from './VehicleDetail/VehicleDetailContainer';
import VehicleEdit from './VehicleEdit';
import Reports from './Reports';
import Breadcrumbs from '../common/breadcrumbs';
import styles from './fleet.css';

const FleetView = ({ locations, vehicles }) => {
  const breadcrumbNameResolvers = {
    locations: (locationId) => (locations[locationId] || {}).name,
    vehicles: (vehicleId) => (vehicles[vehicleId] || {}).vin
  };

  return (
    <div className={styles.FleetView}>
      <nav className={styles.fleetNav}>
        <Nav bsStyle="tabs">
          <LinkContainer to="/fleet/locations">
            <NavItem>Locations</NavItem>
          </LinkContainer>
          <LinkContainer to="/fleet/reports">
            <NavItem>Reports</NavItem>
          </LinkContainer>
        </Nav>
      </nav>
      <Breadcrumbs path="/fleet" nameResolvers={breadcrumbNameResolvers} />
      <Switch>
        <Route exact path="/fleet/locations" component={LocationContainer} />
        <Route path="/fleet/locations/add" component={LocationAdd} />
        <Route exact path="/fleet/locations/:locationId/(view)?" component={LocationDetail} />
        <Route path="/fleet/locations/:locationId/edit" component={LocationEdit} />

        <Route exact path="/fleet/locations/:locationId/vehicles" component={VehicleContainer} />
        <Route path="/fleet/locations/:locationId/vehicles/add" component={VehicleAdd} />
        <Route exact path="/fleet/locations/:locationId/vehicles/:vehicleId/(view)?" component={VehicleDetailContainer} />
        <Route path="/fleet/locations/:locationId/vehicles/:vehicleId/edit" component={VehicleEdit} />

        <Route path="/fleet/reports" component={Reports} />

        <Route>
          <Redirect to="/fleet/locations" />
        </Route>
      </Switch>
    </div>
  );
};

FleetView.propTypes = {
  locations: PropTypes.object,
  vehicles: PropTypes.object
};

const mapStateToProps = (state) => {
  return {
    locations: state.locationData.locations,
    vehicles: state.vehicleData.vehicles
  };
};

export default connect(mapStateToProps)(FleetView);
