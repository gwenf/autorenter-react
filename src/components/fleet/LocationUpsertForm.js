import React from 'react';
import PropTypes from 'prop-types';
import { Col, Form, FormGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import inputField from '../common/form/inputField';
import dropdownField from '../common/form/dropdownField';

let LocationUpsertForm = ({ siteValue, nameValue, cityValue, stateValue, states, action, handleCancel, handleSubmit, submitting, pristine }) => {
  function cancel (e) {
    e.preventDefault();
    handleCancel();
  }

  var statesArray = [];
  if (states) {
    statesArray = [...states.lookupData.states];
  }

  return (
    <div>
      <Form onSubmit={handleSubmit} horizontal>
        <legend>{action} Location</legend>
        <FormGroup controlId="formInputSiteID">
          <Col sm={2}>
            Site ID
          </Col>
          <Col sm={10}>
            <Field
              name="siteId"
              component={inputField}
              required
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="formInputName">
          <Col sm={2}>
            Name
          </Col>
          <Col sm={10}>
            <Field
              name="name"
              component={inputField}
              required
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="formInputCity">
          <Col sm={2}>
            City
          </Col>
          <Col sm={10}>
            <Field
              name="city"
              component={inputField}
              required
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="formInputState">
          <Col sm={2}>
            State
          </Col>
          <Col sm={10}>
            <Field
              name="stateCode"
              states={statesArray}
              component={dropdownField}
              required
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col xsOffset={8} xs={2}>
            <button onClick={cancel} className="btn btn-default pull-right" disabled={submitting}>
              Cancel
            </button>
          </Col>
          <Col xs={2}>
            <button
              type="submit"
              className="btn btn-primary pull-right"
              disabled={!cityValue || !stateValue || !nameValue || !siteValue || pristine || submitting}
            >
              { action === 'Edit' ? 'Update' : 'Submit' }
            </button>
          </Col>
        </FormGroup>
      </Form>
    </div>
  );
};

LocationUpsertForm.propTypes = {
  handleSubmit: PropTypes.func,
  handleCancel: PropTypes.func,
  submitting: PropTypes.bool,
  pristine: PropTypes.bool,
  location: PropTypes.object,
  action: PropTypes.string,
  states: PropTypes.object,
  siteValue: PropTypes.string,
  nameValue: PropTypes.string,
  cityValue: PropTypes.string,
  stateValue: PropTypes.string
};

LocationUpsertForm = reduxForm({
  form: 'UpsertLocation'
})(LocationUpsertForm);

const selector = formValueSelector('UpsertLocation');
LocationUpsertForm = connect((state, props) => {
  const submissionError = state.userData.error;
  const siteValue = selector(state, 'siteId');
  const nameValue = selector(state, 'name');
  const cityValue = selector(state, 'city');
  const stateValue = selector(state, 'stateCode');
  let initialValues = {};
  const { location } = props;
  if (location) {
    initialValues = {
      siteId: location.siteId,
      name: location.name,
      city: location.city,
      stateCode: location.stateCode
    };
  }
  return {
    submissionError,
    initialValues,
    siteValue,
    nameValue,
    cityValue,
    stateValue
  };
})(LocationUpsertForm);

export default LocationUpsertForm;

