import React from 'react';
import PropTypes from 'prop-types';
import style from './locations.css';

const SortButton = ({ onSort, sortBy, currentSort, ...props }) => {
  const current = sortBy === currentSort.sortBy;
  const sortDir = current ? ( currentSort.ascending ? 'asc' : 'desc' ) : '';
  return (
    <button
      className={`${style.headerBtn} ${sortDir}`}
      onClick={() => onSort(sortBy)}
      {...props}
    />
  );
};

SortButton.propTypes = {
  onSort: PropTypes.func.isRequired,
  sortBy: PropTypes.string.isRequired,
  children: PropTypes.node,
  currentSort: PropTypes.shape({
    sortBy: PropTypes.string,
    ascending: PropTypes.bool
  }).isRequired
};

export default SortButton;
