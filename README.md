﻿# AutoRenter - React

A React based implementation of the AutoRenter UI.

The spec is located [here](https://www.gitbook.com/book/fusion-alliance/autorenter-spec/details).

## Overview

These instructions will cover usage information for the UI.

## Prerequisites

The api must also be running for the UI to work properly. Please follow directions in the corresponding readme.

- Make sure the project is at a location with minimal file path length (this is especially important in a Windows environment!). For this project we strongly recommend `c:/aur/ui-react` as the project root.
- Install [Git](https://git-scm.com/downloads).
- Install [Node](https://nodejs.org/en/download/) (tested on version 6.2.2)
- Clone the Git repository to your local machine.
- Install [Chrome](https://www.google.com/chrome/). (Optional - Only needed for debugging)

## How To

**Unless otherwise noted, all terminal commands must be issued from the project's root directory.**

### Install project libraries

```bash
npm install
```

### Run tests

Note that this will lint the code before running tests. No tests will run if lint errors are found.

```bash
npm test
```

Any Jest 'snapshot test' should have the snapshot checked into version control in the src/components/__test__/__snapshots__/ folder.

If you make a visual change to a component, it will no longer match the snapshot and fail the test.  Therefore if you are changing the component intentionally, you should update the snapshot by adding the -u (updateSnapshot) option to the npm test command, along with the specific snapshot test to update:

```bash
npm test -- -u App.test.js
```

you can also update ALL snapshots if desired, by leaving off the filename at the end:

```bash
npm test -- -u
```

### Start the app

To start the app with all debug logging enabled (recommended):

```bash
npm start
```

## Browse the App

After successfully starting the UI app, you should be able to run the application by browsing to [http://127.0.0.1:8080/](http://127.0.0.1:8080/).

## Troubleshooting

### npm "Maximum call stack size exceeded"

- Try running `npm install` again. After a few tries it should finally succeed.

### Everything Is Hosed

Sometimes you just need to completely clean your development environment before starting over from the beginning. The following commands will help you start from a "clean slate":

```bash
# Blow away the node_modules folder:
rimraf node_modules
```

## Contributing

Please read the [CONTRIBUTING](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

- Fusion Alliance for the initiative to create a community of open source development within our ranks.
- Facebook incubator for the [create-react-app package](https://github.com/facebookincubator/create-react-app) that was used to create the boilerplate for this project.